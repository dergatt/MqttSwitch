#include <SPI.h>
#include <PubSubClient.h>
#include <WiFi.h>

include "settings.h"

// Set Pin you want to control in this case build in LED of dev board
const int ledPin = 2;


//Define Pupsub client
  WiFiClient espClient;
  PubSubClient client;


//What happens if Message is recieved
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i=0;i<length;i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  if ((char)payload[0] == '1') digitalWrite(ledPin, HIGH); //set LED On
  if ((char)payload[0] == '0') digitalWrite(ledPin, LOW);  //set LED OFF

}




//reconnect if connection is lost
void reconnect () {
  while (!client.connected()) {
    if (client.connect (device, user, mqtt_password)) {
    Serial.println("Mqtt reconnected");
    client.publish("test/pub","I am here");
    client.subscribe(LED_TOPIC);
  } else {
    Serial.println("wait");
    delay(5000);
    Serial.println("Conection failed");
    Serial.println(client.state());
  }
  }
}




void setup() {
//Set Up serial
  Serial.begin(115200);  
// Set LED
  pinMode(ledPin, OUTPUT);
//set up mqtt
  client.setCallback(callback);
  client.setClient(espClient);
  client.setServer(mqtt_server,1883);

//Wifi
  //Define Wifi
  WiFi.config(ip, gateway, subnet, DNS);
// Connect to Wifi
  WiFi.begin(ssid, password);
while (WiFi.status() != WL_CONNECTED) {
  delay(500);
  Serial.println("Connecting to WiFi..");
};
  Serial.println("Connected to Wifi");



 delay(2500);






}

void loop() {
  //Reconnect to Mqtt
  if (!client.connected()) {
    reconnect();
    Serial.println("Connection Lost");
  }

  client.loop();

}
